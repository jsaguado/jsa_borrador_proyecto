const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe('Firt test',
  function() {
      it('Test that DuckDuckGo works',
        function (done){
            // Method chaining
            // Fluent Interfaces (puedes 'leer' lo que se va a producir)
            // probar que va mal
            // chai.request('http://www.bbva.es/index.jsp')
            chai.request('http://www.duckduckgo.com')
              // recoge un recurso del dominio raiz
              .get('/')
              // nodo terminal (se acaba la fiesta)
              // tiene una funcion manejadora (callback)
              .end(
                function(err, res) {
                    //console.log(res);
                    console.log("Request has finished");
                    console.log(err);
                    // aserción
                    res.should.have.status(200);
                    // contexto asincrono
                    done();
                }
              )
        }
      )
  }
);

describe('Test API Usuarios',
  function() {
      it('Prueba que la que API de usuarios responde correctamente',
        function (done){
            chai.request('http://localhost:3000')
              .get('/apitechu/v1/hello')
              // nodo terminal (se acaba la fiesta)
              // tiene una funcion manejadora (callback)
              .end(
                function(err, res) {
                    console.log("Request has finished");
                    // aserción
                    res.should.have.status(200);
                    res.body.msg.should.be.eql("Hola desde API TechU");

                    done();
                }
              )
        }
      ),
      it('Prueba que la que API devuelve una lista de usuarios correctamente',
        function (done){
            chai.request('http://localhost:3000')
              .get('/apitechu/v1/users')
              .end(
                function(err, res) {
                    console.log("Request has finished");
                    // aserción
                    res.should.have.status(200);
                    res.body.msg.should.be.eql("Hola desde API TechU");

                    done();
                }
              )
        }
      )
  }
);
