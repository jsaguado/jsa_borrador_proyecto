const bcrypt = require('bcrypt');

function hash(data){
   console.log("Hashing data");
   return bcrypt.hashSync(data, 10); //esta no tiene funciñon manejadora porque es síncrona
}

function checkPassword(passwordInPlainText, passwordHashed){
  console.log("checkPassword");

  return bcrypt.compareSync(passwordInPlainText, passwordHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
