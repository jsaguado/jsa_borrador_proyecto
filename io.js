const fs = require('fs');

function writeUserDataToFile(file, data){
  console.log("writeUserDataToFile");
  var jsonData = JSON.stringify(data);

  fs.writeFile(file, jsonData, "utf8", function (err) {
    if (err)
        return console.log(err);
    console.log('Wrote in file ' + file);
  });
}

// exporta la función de nombre interno "writeUserDataToFile" =
// a nombre externo "writeUserDataToFile"
module.exports.writeUserDataToFile = writeUserDataToFile;
