require('dotenv').config();

const io = require('../io');
const requestJSON = require('request-json');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujsa/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountsByUserIdV1(req, res) {

  console.log("GET /apitechu/v1/accounts/:user_id");
  console.log("user_id es " + req.params.id);

  // URL BASE
  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente creado");

  var query = "q={\"user_id\": " + req.params.id + "}";

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    // resMlab - contiene toda la información de la API
    function(err, resMlab, body){
      console.log("resMlab");
      console.log(resMlab);

      if(err){
        var response = {
          "msg" : "Error obteniendo las cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response = {
            "msg" : "No existen cuentas"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

module.exports.getAccountsByUserIdV1 = getAccountsByUserIdV1;
