require('dotenv').config();

const io = require('../io');
const requestJSON = require('request-json');

const crypt = require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujsa/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function postLoginV1(req, res) {
  console.log("POST /apitechu/v1/login");

  var body = req.body;
  // Recogemos el listado de usuarios y passwords
  var users = require('../mock_data_users.json');

  var user = {};
  user.email = body.email;
  user.password = body.password;
  console.log("user: " + JSON.stringify(user));

  var logged = false;
  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.email + " y " +   user.email);
      return element.email == user.email
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    if(users[indexOfElement].password == user.password){
      logged = true;
    }
  }

  var resultado = {}

  if(logged){
    resultado.mensaje="Login correcto";
    resultado.idUsuario=users[indexOfElement].id;

    // actualizamos en el fichero
    users[indexOfElement].logged = true;
    io.writeUserDataToFile("./mock_data_users.json", users);
  }else{
    resultado.mensaje="Login incorrecto";
  }

  res.send(resultado);
}

function postLoginV2(req, res) {
  console.log("POST /apitechu/v2/login");

  var body = req.body;

  var user = {};
  user.email = body.email;
  user.password = body.password;
  console.log("user: " + JSON.stringify(user));

  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente creado");

  var resultado = {}
  var query = "q={\"email\":\"" + user.email + "\"}";

  console.log("Query: " + baseMlabURL + "user?" + query + "&" + mLabAPIKey);

  // Buscamos al usuario en base a su email
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if(err){
        var response = {
          "mensaje" : "Login incorrecto"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var valido = crypt.checkPassword(user.password, body[0].password);
          console.log("Formulario: " + user.password + ", BBDD: " + body[0].password + ", valido: " + valido);
          if(valido === true){
            resultado = {
              "mensaje" : "Login correcto",
              "idUsuario" : body[0].id
            }

            // actualizamos en BBDD
            var putBody = '{"$set":{"logged":true}}';

            httpClient.put("user?" + query + "&" + mLabAPIKey,
               JSON.parse(putBody),
               function(err, resMlab, body){
                 if(err){
                   resultado = {
                     "mensaje" : "Login incorrecto"
                   }
                   res.status(401);
                 }
               }
            )

            res.status(200);
          }else{
            resultado = {
              "mensaje" : "Login incorrecto"
            }
            res.status(401);
          }
        }else{
          resultado = {
            "mensaje" : "Login incorrecto"
          }
          res.status(401);
        }
      }

      res.send(resultado);
    }
  );
}

function postLogoutV1(req, res) {
  console.log("POST /apitechu/v1/logout/:id");

  var idUser = req.params.id;
  console.log("id es " + idUser);

  // Recogemos el listado de usuarios y passwords
  var users = require('../mock_data_users.json');

  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   idUser);
      return element.id == idUser
    }
  )

  var resultado = {
    "mensaje" : "logout incorrecto"
  }

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    if(true === users[indexOfElement].logged){
      delete users[indexOfElement].logged
      io.writeUserDataToFile("./mock_data_users.json", users);
      resultado.mensaje = "logout correcto";
      resultado.idUsuario = users[indexOfElement].id;
    }
  }
  res.send(resultado);
}

function postLogoutV2(req, res) {
  console.log("POST /apitechu/v2/logout/:id");

  var idUser = req.params.id;
  console.log("id es " + idUser);

  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente creado");

  var resultado = {}
  var query = "q={\"id\":" + idUser + "}";

  console.log("Query: " + baseMlabURL + "user?" + query + "&" + mLabAPIKey);

  // Buscamos al usuario en base a su email
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if(err){
        var response = {
          "mensaje" : "logout incorrecto"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          if(body[0].logged === true){
            var putBody = '{"$unset":{"logged":""}}';

            httpClient.put("user?" + query + "&" + mLabAPIKey,
               JSON.parse(putBody),
               function(err, resMlab, body){
                 if(err){
                   resultado = {
                     "mensaje" : "logout incorrecto"
                   }
                   res.status(401);
                 }
               }
            )

            var resultado = {
              "mensaje" : "logout correcto",
              "idUsuario" : body[0].id
            }

            res.status(200);
          }else{
            var response = {
              "mensaje" : "logout incorrecto"
            }
          }
        }else{
          resultado = {
            "mensaje" : "logout incorrecto"
          }
          res.status(401);
        }
      }

      res.send(resultado);
    }
  );
}

module.exports.postLoginV1 = postLoginV1;
module.exports.postLogoutV1 = postLogoutV1
module.exports.postLoginV2 = postLoginV2;
module.exports.postLogoutV2 = postLogoutV2;
