require('dotenv').config();

const io = require('../io');
const requestJSON = require('request-json');

const crypt = require('../crypt');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechujsa/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res) {

  console.log("GET /apitechu/v1/users");
  // '__dirname' variable del directorio actual donde se ejecuta el scripts
  // se utiliza para utilizar urls relativas
  //res.sendFile('mock_data_users.json', {root: __dirname});

  // otra forma de hacer lo mismo
  var users = require('../mock_data_users.json');

  /*
  $top -> Devuelve los N primeros usuarios Ej. $top=5 -> devuelve los 5 primeros usuarios
  $count -> Devuelve un campo "count" con el total de usuarios en la lista si su valor es true.

  OJO - No es el total de usuarios devueltos (pueden ser menos que el total de la lista si se hace un top).
  Ej. $count=true

  Se pueden combinar los dos filtros: $top=5&$count=true

  Opciones - Se puede devolver un object {} en lugar de un array [] en la respuesta.

  Explorar Array.splice vs Array.slice
  Ejemplos de URL de petición:

  localhost:3000/apitechu/v1/users?$top=10&$count=true
  localhost:3000/apitechu/v1/users?$top=3
  localhost:3000/apitechu/v1/users?$count=true
  */
  var resultado = {
    "users" : []
  };

  var top = req.query.$top; // $_GET["$top"]
  var count = req.query.$count; // $_GET["$count"]

  if(top){
    // no se comprueba si es <= 0 o > Lenght[users]
    console.log("top: " + top);
    resultado.users = users.slice(0, top);
  }else{
    resultado.users = users;
  }

  if(count && count == 'true'){
    console.log("count: " + count);
    resultado = {
        "users" : resultado.users,
        "count" : users.length
    }
  }

  res.send(resultado);
}

function getUsersV2(req, res) {

  console.log("GET /apitechu/v2/users");

  // URL BASE
  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + mLabAPIKey,
    // resMlab - contiene toda la información de la API
    function(err, resMlab, body){
      //console.log("resMlab");
      //console.log(resMlab);
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  );
}

function getUserByIdV2(req, res) {

  console.log("GET /apitechu/v2/users/:id");
  console.log("id es " + req.params.id);

  // URL BASE
  var httpClient = requestJSON.createClient(baseMlabURL);
  console.log("Cliente creado");

  var query = "q={\"id\": " + req.params.id + "}";

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    // resMlab - contiene toda la información de la API
    function(err, resMlab, body){
      //console.log("resMlab");
      //console.log(resMlab);
      /*
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      */
      if(err){
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }

      res.send(response);
    }
  );
}

function postUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");

  // console.log(req.headers);
  /*
  { first_name: 'Tony',
    last_name: 'Stark',
    email: 'tony@stark.com',
    'cache-control': 'no-cache',
    'postman-token': '243d8a66-03bf-4f12-821c-06b7592df4fa',
    'user-agent': 'PostmanRuntime/7.4.0',
    accept: '* / *',
    host: 'localhost:3000',
    'accept-encoding': 'gzip, deflate',
    'content-length': '0',
    connection: 'keep-alive' }
  */

  var newUser = {};
  var body = req.body;
  var users = require('../mock_data_users.json');

  newUser.id = users.length +1;
  newUser.first_name = body.first_name;
  newUser.last_name = body.last_name;
  newUser.email = body.email;
  newUser.password = body.password;
  console.log("user: " + JSON.stringify(newUser));

  users.push(newUser);
  io.writeUserDataToFile("./mock_data_users.json", users);
}

function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");

  var usersLen = 0;
  var httpClient = requestJSON.createClient(baseMlabURL);

  // obtenemos el ID

  var query = "s={\"id\": -1}&l=-1";

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if(!err){
        if(body.length > 0){
          console.log("body");
          console.log(body[0].id);
          usersLen = body[0].id +1;
        }
      }

      var newUser = {};
      var body = req.body;

      newUser.id = usersLen;
      newUser.first_name = body.first_name;
      newUser.last_name = body.last_name;
      newUser.email = body.email;
      newUser.password = crypt.hash(req.body.password);

      console.log("user: " + JSON.stringify(newUser));

      httpClient.post("user/?"+mLabAPIKey, newUser,
        function(err, resMLab, body){
          console.log("Usuario guardado con éxito");
          console.log(body);
          res.status(201).send({"msg" : "usuario guardado"});
        }
      )
    }
  );
}

function deleteUsersV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../mock_data_users.json');
  var deleted = false;

  // console.log("Usando for normal");
  // for (var i = 0; i < users.length; i++) {
  //   console.log("comparando " + users[i].id + " y " +  req.params.id);
  //   if (users[i].id == req.params.id) {
  //     console.log("La posicion " + i + " coincide");
  //     users.splice(i, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for in");
  // for (arrayId in users) {
  //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
  //   if (users[arrayId].id == req.params.id) {
  //     console.log("La posicion " + arrayId " coincide");
  //     users.splice(arrayId, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of");
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición ? coincide");
  //     // Which one to delete? order is not guaranteed...
  //     deleted = false;
  //     break;
  //   }
  // }

  // console.log("Usando for of 2");
  // // Destructuring, nodeJS v6+
  // for (var [index, user] of users.entries()) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  // }

  // console.log("Usando for of 3");
  // var index = 0;
  // for (user of users) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posición " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //     break;
  //   }
  //   index++;
  // }

  // console.log("Usando Array ForEach");
  // users.forEach(function (user, index) {
  //   console.log("comparando " + user.id + " y " +  req.params.id);
  //   if (user.id == req.params.id) {
  //     console.log("La posicion " + index + " coincide");
  //     users.splice(index, 1);
  //     deleted = true;
  //   }
  // });

  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   req.params.id);
      return element.id == req.params.id
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    users.splice(indexOfElement, 1);
    deleted = true;
  }

  if (deleted) {
    // io.writeUserDataToFile(users);
    io.writeUserDataToFile("./mock_data_users.json", users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}

function deleteUsersV2(req, res) {
  console.log("DELETE /apitechu/v2/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../mock_data_users.json');
  var deleted = false;

  console.log("Usando Array findIndex");
  var indexOfElement = users.findIndex(
    function(element){
      console.log("comparando " + element.id + " y " +   req.params.id);
      return element.id == req.params.id
    }
  )

  console.log("indexOfElement es " + indexOfElement);
  if (indexOfElement >= 0) {
    users.splice(indexOfElement, 1);
    deleted = true;
  }

  if (deleted) {
    // io.writeUserDataToFile(users);
    io.writeUserDataToFile("./mock_data_users.json", users);
  }

  var msg = deleted ?
  "Usuario borrado" : "Usuario no encontrado."

  console.log(msg);
  res.send({"msg" : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;

module.exports.postUsersV1 = postUsersV1;
module.exports.createUserV2 = createUserV2;

module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.deleteUsersV2 = deleteUsersV2;
