console.log("Iniializando");

// Disponibilizamos el framework de express y lo inicializamos
const express = require('express');
const app = express();

const io = require('./io');

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

var enableCORS = function(req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  // Se deberá habilitar la cabecera del token JWT (si empieza por X- es una cabecera que es propietaria)
  next();
}

// se indica que el body siempre es de tipo JSON
app.use(express.json());

// permite la invocación desde distintos dominios
app.use(enableCORS);

// las variables de entorno se ponen en mayusculas
const port = process.env.PORT || 3000;

app.listen(port);
console.log("API escuchando en el puerto BIP BIP " + port);

// el texto entre las comillas simples son siempre texto
// todas las respuestas del curso deben ser JSON
app.get ('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }
);

// Ejemplo de peticiones
// - Params - variables necesarias para obtener el resultado
// - QueryString - variales opcionales (GET)
// - Headers - parametros de configuracion
// - Body - parametros obligtorios (PUT, POST, PATCH)
app.post('/apitechu/v1/ejemplo/:p1/:p2',
  function(req, res) {
    console.log("POST /apitechu/v1/ejemplo");

    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
);

// CRUD - READ - GET
// userController.getUsersV1 - Es como si estuviera poniendo la funcion
app.get ('/apitechu/v1/users', userController.getUsersV1);

app.get ('/apitechu/v2/users', userController.getUsersV2);

app.get ('/apitechu/v2/users/:id', userController.getUserByIdV2);

// CRUD - CREATE - POST
app.post('/apitechu/v1/users', userController.postUsersV1);

app.post('/apitechu/v2/users', userController.createUserV2);

// CRUD - DELETE - DELETE
app.delete('/apitechu/v1/users/:id', userController.deleteUsersV1);

app.delete('/apitechu/v2/users/:id', userController.deleteUsersV2);

app.post('/apitechu/v1/login', authController.postLoginV1);
app.post('/apitechu/v1/logout/:id', authController.postLogoutV1);

app.post('/apitechu/v2/login', authController.postLoginV2);
app.post('/apitechu/v2/logout/:id', authController.postLogoutV2);

app.get ('/apitechu/v1/accounts/:id', accountController.getAccountsByUserIdV1);
